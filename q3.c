#include <stdio.h>
void read(int matrix[][10], int , int );
void add(int m1[][10],int m2[][10],int sum[][10], int , int , int , int );
void multiply(int mat01[][10],int mat02[][10],int result[][10], int , int , int , int );
void answer(int result[][10], int , int );

void read(int matrix[][10], int row, int column)
{
    printf("\nEnter Elements to Matrix\n");

   for (int i = 0; i < row; ++i) {
      for (int j = 0; j < column; ++j) {
         printf("Enter element %d of row %d: ", j + 1, i + 1);
         scanf("%d", &matrix[i][j]);
      }
   }
}
void add(int m1[][10],int m2[][10],int sum[][10], int r1, int c1, int r2, int c2)
{
    for (int i = 0; i < r1; ++i)
        for (int j = 0; j < c2; ++j) {
            sum[i][j] = m1[i][j] + m2[i][j];
        }

    printf("\nAnswer of Addition\n");
    for (int i = 0; i < r1; ++i)
        for (int j = 0; j < c2; ++j) {
            printf("%d   ", sum[i][j]);
                if (j == c2 - 1) {
                printf("\n");
                }
        }
}
void multiply(int mat01[][10],int mat02[][10],int result[][10], int r1, int c1, int r2, int c2)
{
    // Initializing elements of matrix Multiplication to 0
    for (int i = 0; i < r1; ++i) {
      for (int j = 0; j < c2; ++j) {
         result[i][j] = 0;
      }
   }

   for (int i = 0; i < r1; ++i) {
      for (int j = 0; j < c2; ++j) {
         for (int k = 0; k < c1; ++k) {
            result[i][j] += mat01[i][k] * mat02[k][j];
         }
      }
   }
}

void answer(int result[][10], int r, int c) {
   printf("\nAnswer of Multiplication\n");
   for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
         printf("%d  ", result[i][j]);
         if (j == c - 1)
            printf("\n");
      }
   }
}

int main() {
   int m1[10][10], m2[10][10], result[10][10], row1, col1, row2, col2;
   printf("Enter number of ROWS for MATRIX 01 : ");
   scanf("%d", &row1);
   printf("Enter number of COLUMNS for MATRIX 01 : ");
   scanf("%d",&col1);
   printf("\nEnter number of ROWS for MATRIX 02 : ");
   scanf("%d",&row2);
   printf("Enter number of COLUMNS for MATRIX 02 : ");
   scanf("%d",&col2);

   while (col1 != row2) {

    printf("\nError! Columns of 1st matrix should equal to Rows of 2nd matrix!!!\nPlease enter rows and columns again.\n\n");
    printf("Enter number of ROWS for MATRIX 01 : ");
    scanf("%d %d", &row1, &col1);
    printf("Enter number of COLUMNS for MATRIX 01 : ");
    scanf("%d %d", &row2, &col2);
    printf("\nEnter number of ROWS for MATRIX 02 : ");
    scanf("%d %d", &row1, &col1);
    printf("Enter number of COLUMNS for MATRIX 02 : ");
    scanf("%d %d", &row2, &col2);

   }
   read(m1, row1, col1);// Read elements to matrix 01
   read(m2, row2, col2);// Read elements to matrix 02
   add(m1, m2, result, row1, col1, row2, col2);// Addition Part (matrix 01 + matrix 02)
   multiply(m1, m2, result, row1, col1, row2, col2); // Multiplication Part (matrix 01 * matrix 02)
   answer(result, row1, col2);//Answer

   return 0;
}
