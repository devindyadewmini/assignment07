#include <stdio.h>
#include <string.h>
#define SIZE 1000

int main()
{
    int i,len;
    int count=0;
    char x;
    char text[SIZE];

    printf ("Enter the String - \n");
    fgets(text,SIZE,stdin);

    printf ("\nEnter the character you want to get frequency - \n");
    scanf("%c",&x);

    len = strlen(text);
    //printf("%d\n",len);

    for (i=0; i<len; i=i+1)
    {
        if (x==text[i])
            count = count + 1;
    }

    printf("\nFrequency of '%c' in entered string = %d\n",x,count);

    return 0;

}
