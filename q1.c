#include <stdio.h>
#include <string.h>
#define SIZE 1000

int main()
{
    int len;
    int i;
    char text[SIZE];
    printf ("Enter the sentence you want to get Reverse - \n");
    fgets(text,SIZE,stdin);
    len = strlen(text);
    //printf("%d\n",len);

    printf ("\nReverse of Entered Sentence - ");
    for (i = len-1; i >=0 ; i=i-1)
    {
        printf("%c",text[i]);
    }
    printf("\n\n");

    return 0;
}
